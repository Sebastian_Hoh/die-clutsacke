package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Rectangle;

public class KisteOeffnen extends JFrame {

	private JPanel contentPane;

	public KisteOeffnen() {
		setTitle("Kiste \u00F6ffnen");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(new Rectangle(600, 300, 500, 300));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JButton btnOeffnen = new JButton("\u00F6ffnen");
		contentPane.add(btnOeffnen, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblItem = new JLabel("*Item*");
		lblItem.setHorizontalTextPosition(SwingConstants.CENTER);
		lblItem.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblItem);
	}

}
