package GUI;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollBar;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Rectangle;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Dimension;
import java.awt.Component;
import javax.swing.JSplitPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Inventar extends JFrame {

	private JPanel contentPane;
	private JTextField textItemZahl;
	private JTextField txtGoldanzeige;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public Inventar() {
		setTitle("Inventar");
		setBounds(new Rectangle(600, 300, 500, 300));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollBar scrollBar = new JScrollBar();
		
		contentPane.add(scrollBar, BorderLayout.EAST);
		
		JButton btnZurueck = new JButton("zur\u00FCck");
		btnZurueck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Befehle.hauptmenüAufrufen();
			}
		});
		contentPane.add(btnZurueck, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		JButton lblItem = new JButton("*Item*");
		lblItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Befehle.PopupItemVerwaltenAufrufen();
			}
		});
		lblItem.setHorizontalAlignment(SwingConstants.CENTER);
		lblItem.setAlignmentY(Component.TOP_ALIGNMENT);
		lblItem.setHorizontalTextPosition(SwingConstants.CENTER);
		lblItem.setMinimumSize(new Dimension(100, 100));
		lblItem.setMaximumSize(new Dimension(102, 102));
		lblItem.setPreferredSize(new Dimension(100, 100));
		panel.add(lblItem);
		
		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.NORTH);
		
		textItemZahl = new JTextField();
		textItemZahl.setText("??? / ??? ");
		textItemZahl.setHorizontalAlignment(SwingConstants.CENTER);
		textItemZahl.setEditable(false);
		textItemZahl.setColumns(10);
		splitPane.setRightComponent(textItemZahl);
		
		txtGoldanzeige = new JTextField();
		txtGoldanzeige.setEditable(false);
		txtGoldanzeige.setHorizontalAlignment(SwingConstants.CENTER);
		txtGoldanzeige.setText("Goldanzeige");
		splitPane.setLeftComponent(txtGoldanzeige);
		txtGoldanzeige.setColumns(10);
	}

}
