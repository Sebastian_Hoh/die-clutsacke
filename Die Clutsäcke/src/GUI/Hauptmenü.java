package GUI;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import java.awt.Rectangle;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Hauptmen� extends JFrame {
	
	public Hauptmen�() {
		setBounds(new Rectangle(600, 300, 500, 300));
		setName("Hauptmen�");
		
		JButton btnZufaelligeKiste = new JButton("Zuf\u00E4llige Kiste");
		getContentPane().add(btnZufaelligeKiste, BorderLayout.WEST);
		
		JButton btnInventar = new JButton("Inventar");
		btnInventar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Befehle.inventarAufrufen();
			}
		});
		getContentPane().add(btnInventar, BorderLayout.EAST);
		
		JButton btnKisteOeffnen = new JButton("Kiste \u00F6ffnen");
		btnKisteOeffnen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				Befehle.KisteOeffnenAufrufen();
			}
		});
		getContentPane().add(btnKisteOeffnen, BorderLayout.CENTER);
		
		JSplitPane ausloggenShop = new JSplitPane();
		getContentPane().add(ausloggenShop, BorderLayout.SOUTH);
		
		JButton btnAusloggen = new JButton("    ausloggen    ");
		ausloggenShop.setLeftComponent(btnAusloggen);
		
		JButton btnShop = new JButton("Shop");
		btnShop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Befehle.ShopAufrufen();
			}
		});
		ausloggenShop.setRightComponent(btnShop);
		
	}
	
	
	
	
}