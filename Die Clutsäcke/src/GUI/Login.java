package GUI;

import Logic.Logik;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Login extends JFrame {
	private JTextField Name;
	private JTextField txtPasswort;
	private JPasswordField passwort;
	private JTextField username;
	Logik logik = new Logik();
	public Login() {
		
		setBounds(new Rectangle(600, 300, 500, 300));
		setVisible(true);
		setTitle("Kistensimulator");
		
		Name = new JTextField();
		Name.setHorizontalAlignment(SwingConstants.CENTER);
		Name.setEditable(false);
		Name.setText("Name:");
		Name.setColumns(10);
		
		txtPasswort = new JTextField();
		txtPasswort.setHorizontalAlignment(SwingConstants.CENTER);
		txtPasswort.setEditable(false);
		txtPasswort.setText("Passwort:");
		txtPasswort.setColumns(10);
		
		passwort = new JPasswordField();
		passwort.setColumns(10);
		
		username = new JTextField();
		username.setColumns(10);
		
		JButton btnRegister = new JButton("registrieren");
		btnRegister.setPreferredSize(new Dimension(110, 23));
		btnRegister.setMaximumSize(new Dimension(110, 23));
		btnRegister.setMinimumSize(new Dimension(110, 23));
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				Befehle.registerAufrufen();
			}
		});
		
		JButton btnBestaetigen = new JButton("best\u00E4tigen");
		btnBestaetigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			logik.login(username.getText(), passwort.getText());
			}
		});
		
		btnBestaetigen.setPreferredSize(new Dimension(110, 23));
		btnBestaetigen.setMaximumSize(new Dimension(110, 23));
		btnBestaetigen.setMinimumSize(new Dimension(110, 23));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(Name)
						.addComponent(txtPasswort, GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(username, GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
						.addComponent(passwort, GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE))
					.addContainerGap())
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(105)
					.addComponent(btnRegister)
					.addPreferredGap(ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
					.addComponent(btnBestaetigen)
					.addGap(105))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(83)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(Name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(username, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(40)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(passwort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnRegister)
						.addComponent(btnBestaetigen))
					.addGap(30))
		);
		getContentPane().setLayout(groupLayout);
		
	}
}
