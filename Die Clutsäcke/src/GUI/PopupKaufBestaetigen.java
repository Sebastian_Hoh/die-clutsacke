package GUI;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PopupKaufBestaetigen extends JFrame {

	private JPanel contentPane;
	private JTextField txtKaufbestaetigung;

	public PopupKaufBestaetigen() {
		setType(Type.POPUP);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(700, 350, 300, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		txtKaufbestaetigung = new JTextField();
		txtKaufbestaetigung.setEditable(false);
		txtKaufbestaetigung.setHorizontalAlignment(SwingConstants.CENTER);
		txtKaufbestaetigung.setText("Wollen Sie dieses Item wirklich kaufen?");
		contentPane.add(txtKaufbestaetigung, BorderLayout.CENTER);
		txtKaufbestaetigung.setColumns(10);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnKaufen = new JButton("kaufen");
		panel.add(btnKaufen);
		
		JButton btnZur�ck = new JButton("zur\u00FCck");
		btnZur�ck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		panel.add(btnZur�ck);
	}

}
