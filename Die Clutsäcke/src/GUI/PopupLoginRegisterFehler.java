package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Window.Type;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSplitPane;

public class PopupLoginRegisterFehler extends JFrame {

	private JPanel contentPane;
	private JTextField txtNameOderPasswort;

	public PopupLoginRegisterFehler() {
		setResizable(false);
		setTitle("Fehler");
		setType(Type.POPUP);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(700, 350, 300, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnOkay = new JButton("okay");
		btnOkay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		panel.add(btnOkay);
		
		txtNameOderPasswort = new JTextField();
		txtNameOderPasswort.setEditable(false);
		txtNameOderPasswort.setHorizontalAlignment(SwingConstants.CENTER);
		txtNameOderPasswort.setText("Name oder Passwort ist falsch!");
		contentPane.add(txtNameOderPasswort, BorderLayout.CENTER);
		txtNameOderPasswort.setColumns(10);
	}

}
