package GUI;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollBar;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Rectangle;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Shop extends JFrame {

	private JPanel contentPane;
	private JTextField txtGoldanzeige;

	
	public Shop() {
		setTitle("Shop");
		setBounds(new Rectangle(600, 300, 500, 300));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollBar scrollBar = new JScrollBar();
		
		contentPane.add(scrollBar, BorderLayout.EAST);
		
		JButton btnZurueck = new JButton("zur\u00FCck");
		btnZurueck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Befehle.hauptmenüAufrufen();
			}
		});
		contentPane.add(btnZurueck, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
		
		JButton btnItem = new JButton("*Item*");
		btnItem.setAlignmentY(Component.TOP_ALIGNMENT);
		btnItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Befehle.PopupKaufBestaetigenAufrufen();
			}
		});
		btnItem.setMinimumSize(new Dimension(100, 100));
		btnItem.setMaximumSize(new Dimension(100, 100));
		btnItem.setPreferredSize(new Dimension(100, 100));
		panel.add(btnItem);
		
		JButton btnItem2 = new JButton("*Item*");
		btnItem2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Befehle.PopupKaufBestaetigenAufrufen();
			}
		});
		btnItem2.setPreferredSize(new Dimension(100, 100));
		btnItem2.setMinimumSize(new Dimension(100, 100));
		btnItem2.setMaximumSize(new Dimension(100, 100));
		btnItem2.setAlignmentY(0.0f);
		panel.add(btnItem2);
		
		txtGoldanzeige = new JTextField();
		txtGoldanzeige.setEditable(false);
		txtGoldanzeige.setHorizontalAlignment(SwingConstants.CENTER);
		txtGoldanzeige.setText("Goldanzeige");
		contentPane.add(txtGoldanzeige, BorderLayout.NORTH);
		txtGoldanzeige.setColumns(10);
	}

}
