package GUI;

import Logic.Logik;
import Data.SQL_Anbindung;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.fabric.xmlrpc.base.Array;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Point;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JPasswordField;

public class Register extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtPasswort;
	private JTextField txtPasswortBestaetigen;
	private JTextField username;
	private JTextField passwort;
	private JTextField passwort2;
	public Logik logik = new Logik();
	public SQL_Anbindung sql = new SQL_Anbindung();

	public Register() {
		
		
		setTitle("registrieren");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(600, 300, 500, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnZurueck = new JButton("zur\u00FCck");
		btnZurueck.setPreferredSize(new Dimension(110, 23));
		btnZurueck.setMinimumSize(new Dimension(110, 23));
		btnZurueck.setMaximumSize(new Dimension(110, 23));
		btnZurueck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Befehle.loginAufrufen();
			}
		});
		
		JButton btnBestaetigen = new JButton("best\u00E4tigen");
		btnBestaetigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(sql.anmelden(username.getText(), passwort.getText()) == false) {
				if (passwort.getText() == passwort2.getText()) {
					logik.BenutzerEintragen(username.getText(), passwort.getText());
					
				}
				else {
					Befehle.PopupLoginRegisterFehlerAufrufen();
					System.out.println("XD");
				}
			} else {
				Befehle.PopupLoginRegisterFehlerAufrufen();
			}
			}	
			
			});
		btnBestaetigen.setMaximumSize(new Dimension(110, 23));
		btnBestaetigen.setMinimumSize(new Dimension(110, 23));
		btnBestaetigen.setPreferredSize(new Dimension(110, 23));
		
		txtName = new JTextField();
		txtName.setHorizontalAlignment(SwingConstants.CENTER);
		txtName.setText("Name:");
		txtName.setEditable(false);
		txtName.setColumns(10);
		
		txtPasswort = new JTextField();
		txtPasswort.setEditable(false);
		txtPasswort.setText("Passwort:");
		txtPasswort.setHorizontalAlignment(SwingConstants.CENTER);
		txtPasswort.setColumns(10);
		
		txtPasswortBestaetigen = new JTextField();
		txtPasswortBestaetigen.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txtPasswortBestaetigen.setHorizontalAlignment(SwingConstants.CENTER);
		txtPasswortBestaetigen.setLocation(new Point(1, 0));
		txtPasswortBestaetigen.setEditable(false);
		txtPasswortBestaetigen.setText("Passwort:");
		txtPasswortBestaetigen.setColumns(10);
		
		username = new JTextField();
		username.setColumns(10);
		
		passwort = new JTextField();
		passwort.setColumns(10);
		
		passwort2 = new JTextField();
		passwort2.setColumns(10);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnZurueck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(57)
							.addComponent(btnBestaetigen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(105))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(txtName, Alignment.LEADING)
								.addComponent(txtPasswort, Alignment.LEADING)
								.addComponent(txtPasswortBestaetigen, Alignment.LEADING))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(passwort, 318, 318, Short.MAX_VALUE)
								.addComponent(username, GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE)
								.addComponent(passwort2, GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE))
							.addGap(31))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(55)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(username, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(passwort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtPasswortBestaetigen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(passwort2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnBestaetigen, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnZurueck, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(19))
		);
		contentPane.setLayout(gl_contentPane);
	}
}