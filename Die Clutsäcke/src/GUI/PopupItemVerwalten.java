package GUI;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PopupItemVerwalten extends JFrame {

	private JPanel contentPane;
	private JTextField txtVerkauf;

	public PopupItemVerwalten() {
		setType(Type.POPUP);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(700, 350, 300, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		txtVerkauf = new JTextField();
		txtVerkauf.setEditable(false);
		txtVerkauf.setHorizontalAlignment(SwingConstants.CENTER);
		txtVerkauf.setText("Wollen Sie dieses Item wirklich verkaufen?");
		contentPane.add(txtVerkauf, BorderLayout.CENTER);
		txtVerkauf.setColumns(10);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnVerkaufen = new JButton("verkaufen");
		panel.add(btnVerkaufen);
		
		JButton btnZur�ck = new JButton("zur\u00FCck");
		btnZur�ck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		panel.add(btnZur�ck);
	}

}
