package GUI;

import java.awt.EventQueue;

public class Befehle {
	
public static void loginAufrufen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login Loginbereich = new Login();
					Loginbereich.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void registerAufrufen() {	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register frame = new Register();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void hauptmenüAufrufen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Hauptmenü frame = new Hauptmenü();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void inventarAufrufen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inventar frame = new Inventar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void ShopAufrufen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Shop frame = new Shop();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void PopupKaufBestaetigenAufrufen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupKaufBestaetigen frame = new PopupKaufBestaetigen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void PopupItemVerwaltenAufrufen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupItemVerwalten frame = new PopupItemVerwalten();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void KisteOeffnenAufrufen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KisteOeffnen frame = new KisteOeffnen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void PopupLoginRegisterFehlerAufrufen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopupLoginRegisterFehler frame = new PopupLoginRegisterFehler();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}