package Logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 

public class test {
 
    private String db_host = "localhost";
    private String db_port = "3306";
    private String db_user = "root";
    private String db_pass = "";
    private String db_base = "kistensimulator?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
 
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private Connection connection = null;
 

    public void createConnection() throws SQLException, ClassNotFoundException {
        Class.forName(DRIVER);
        String mySqlUrl = "jdbc:mysql://" + db_host + ":" + db_port + "/"
                + db_base;
        connection = DriverManager.getConnection(mySqlUrl, db_user, db_pass);
    }
 
  
    public void showAll(String nameOfTable) throws SQLException {
        String query = "SELECT ID, Name, Seltenheitgrad, Wert FROM " + nameOfTable;
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        int columns = rs.getMetaData().getColumnCount();
        for (int i = 1; i <= columns; i++)
            System.out.print(rs.getMetaData().getColumnLabel(i) + "\t\t");
        System.out.println();
        System.out.println();
        while (rs.next()) {
            for (int i = 1; i <= columns; i++) {
                System.out.print(rs.getString(i) + "\t\t");
            }
            System.out.println();
        }
        rs.close();
        stmt.close();
    }
 
    public void closeProgramm() throws SQLException {
        this.connection.close();
    }
 
    public static void main(String[] args) {
        test app = new test();
        try {

            app.createConnection();

            app.showAll("items");

            app.closeProgramm();
        } catch (SQLException e) {
            System.err.println("SQL Fehler - " + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.err.println("Der Datenbank treiber wurde nicht gefunden. - "
                    + e.getLocalizedMessage());
            e.printStackTrace();
        }
        System.exit(0);
    }
}
