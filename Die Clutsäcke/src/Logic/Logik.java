package Logic;

import Data.SQL_Anbindung;
import GUI.Befehle;

//import Data.SQL_AnbindungLG;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JLabel;



public class Logik {
	
	private SQL_Anbindung dconn;
	private String currentUser;
	private String messageTextLogIn;

	public Logik() {
		this.dconn = new SQL_Anbindung();
		this.currentUser = null;
		this.messageTextLogIn = "No current User";
	}

	public String getMessageTextLogIn() {
		return messageTextLogIn;
	}

	public String getCurrentUser() {
		return currentUser;
	}

/*	public boolean registerUser(String username, String passwort) {
		return dconn.userEintragen(username, passwort);
	} */

	public boolean login(String username, String passwort) {
		if (userExists(username, passwort)) {
			this.currentUser = username;
			this.messageTextLogIn = "LogIn erfolgreich";
			return true;
		} else {
			this.messageTextLogIn = "User existiert nicht!";
			return false;
		}
	}

	public void logout(String username, String passwort) {
		this.currentUser = null;
		this.messageTextLogIn = "ausgeloggen erfolgreich";
	}

	public boolean userExists(String username, String passwort) {
		return dconn.anmelden(username, passwort);
	}

	public void BenutzerEintragen(String username, String passwort ) {
		//if((username !="" && passwort != "" && userExists(username,passwort) == false)) {
		//if(dconn.anmelden(username, passwort) == true)	{
		dconn.userEintragen(username, passwort);		
		//}
		
	}

	public boolean passwortVergleich(String passwort, String passwort2) {
		if(passwort == passwort2) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/*public String[][] getBenutzer(String ACC_ID, String Item_ID) {
		List<Account> liste = dconn.userEintragen(ACC_ID, Item_ID);
		String[][] benutzerarray = new String[liste.size()][3];
		int index = 0;
		for(Benutzer m: liste){
			benutzerarray[index][0] = m.getName();
			benutzerarray[index][1] = m.getPasswort();
			benutzerarray[index][2] = m.getGold(500);
			index++;
		}
		return benutzerarray;
	} */

	public void itemEintragen(String ACC_ID, String Item_ID) {
		//(if(!userExists(username, passwort))
			//registerUser(username, passwort);

		dconn.getInventar(ACC_ID);
		itemEintragen(ACC_ID, Item_ID);
		//dconn.ItemKaufen(i.getItemName(), getCurrentUser(), i.getWert());
	}
	
	
	public void itemKaufen(String ACC_ID, String Item_ID) {
	//	if(!userExists(username, passwort))
		//	registerUser(username, passwort);

		dconn.getInventar(ACC_ID);
		//itemEintragen(username, passwort, i);
		dconn.ItemKaufen(Item_ID, ACC_ID, dconn.ItemGold(Item_ID));
	} 
	
	
	public void itemVerkaufen(String ACC_ID, String Item_ID) {
		//if(!userExists(username, passwort))
			//registerUser(username, passwort);

		dconn.getInventar(ACC_ID);
		//itemEintragen(username, passwort, i);
		dconn.ItemVerkaufen(Item_ID, ACC_ID, dconn.ItemGold(Item_ID));;
	}
	
	
	public void kisteEintragen(String ACC_ID, String Kiste_ID) {
	//	if(!userExists(username, passwort))
	//	registerUser(username, passwort);

		dconn.getInventar(ACC_ID);
		kisteEintragen(Kiste_ID, ACC_ID);
		//dconn.KisteKaufen(k.getKistenArt(), getCurrentUser(), k.getKistenWert());
	}
	
	public void kistenKaufen(String ACC_ID, String Kiste_ID) {
	//	if(!userExists(username, passwort))
		//	registerUser(username, passwort);
		
		dconn.getInventar(ACC_ID);
		dconn.KisteKaufen(Kiste_ID, ACC_ID, dconn.KistenGold(Kiste_ID));
	} 

	
	public void kistenVerkaufen(String ACC_ID, String Kiste_ID) {
		
		dconn.getInventar(ACC_ID);
		dconn.KisteVerkaufen(Kiste_ID, ACC_ID, dconn.KistenGold(Kiste_ID));
		}
	
	public void inventarAnzeigen(String ACC_ID) {
		dconn.getInventar(ACC_ID);
	}
	
	/*public String[][] getKiste() {	
		return dconn.getKiste(getCurrentUser());
	}	*/
	
	public String[][] getKiste(String ACC_ID) {	
		return dconn.getKiste(ACC_ID);
	}
	
	public void shopAnzeigen() {
		dconn.zeigeItems();
		dconn.zeigekisten();
	}
	
	public void zeigeItems(String item_ID, int width, int height, JLabel Label2) {
		dconn.zeigeItemBild(item_ID, width, height, Label2);
	}
	
	public void kiste�ffnen(String Kisten_ID) {
		dconn.getKiste(Kisten_ID);
		int zahl = (int)(Math.random() * 14 + 1);
		int goldzahl = 30;
		int silberzahl = 15;
		
		  if(Kisten_ID == "Goldkiste") {
		  int Item_ID = goldzahl + zahl;
		  String Item_ID2 = String.valueOf(Item_ID);
	  		dconn.zeigeItem(Item_ID2);
		  }
		  if(Kisten_ID == "Silberkiste") {
		  		int Item_ID = silberzahl + zahl;
		  		String Item_ID2 = String.valueOf(Item_ID);
		  		dconn.zeigeItem(Item_ID2);
		  	}
		  			else{ int Item_ID = zahl;
		  			String Item_ID2 = String.valueOf(Item_ID);
			  		dconn.zeigeItem(Item_ID2);
		  	}
		
			
		}
	
	public void zuf�lligeKiste() {
		int zahl = (int)(Math.random() * 9 + 1);
		if(zahl < 7) {
			dconn.getKiste("1");
		}
		else {
			dconn.getKiste("2");
		}
	}
		
	
	

/*	SQL_AnbindungLG s = new SQL_AnbindungLG();
		SQL_AnbindungLG data;
	Benutzer Benutzer = new Benutzer();
	Gold Gold = new Gold();
	Items Items = new Items();
	Kiste Kiste = new Kiste();	
	
	public Logik(SQL_AnbindungLG data) {
		this.data = data;
	}
	
	private Logik hh = new Logik();	
	
	public void items_Eintragen(Items i) {
		hh.items_Eintragen(i);
	
	}
	public void kiste_Eintragen(Kiste k) {
		hh.kiste_Eintragen(k); 
	
	}
	public void account_Eintragen(Benutzer b) {
		hh.account_Eintragen(b); 
	
	}
	
 	public void Loggin(boolean �bereinstimmung) {
		if(data.account_name  == "" && data.passwort == "") {
		//	if(name "Admin" && passwort == "Admin") { 
		//}													// Nacher f�r den Admin
			�bereinstimmung = true;
			return;
		}
		else {
			�bereinstimmung = false;
			return;
		}
	}
	
	//Registrier Methode
	public void Registrieren() {
		if(Benutzer.name == "") {
			System.out.println("Der Name ist Bereits vorhanden! /n Bitte geben Sie einen neue Namen ein! ");
		} else {
			Benutzer.name = "";  
			Benutzer.passwort = "";
		}
		
	}
	
	public void KistenKaufen() {
		if((Gold.goldstand-Kiste.kistenkosten)<0) {
			System.out.println("Du hast nicht genug Gold um dir diese Kiste zu holen ");
		} else {
			Kiste.chest++;
			Gold.goldstand = (Gold.goldstand - Kiste.kistenkosten);
			return; }
		}
	
	public void ItemKaufen() {
		if((Gold.goldstand-Items.Itemkosten)<0) {
			System.out.println("Du hast nicht genug Gold um dir dieses Item zu holen ");
		} else {
			data.getItem(ID);
			Gold.goldstand = (Gold.goldstand - Items.Itemkosten);
			return; }
		}

	public void Zufallskack() {
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
  */
}
