package Data;
import java.awt.Image;
import java.awt.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Logic.test;


public class SQL_Anbindung {

	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/kistensimulator?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private String user = "root";
	private String password = "";


	public boolean userEintragen(String username, String Passwort) {
		String sql = ("INSERT INTO account (Name, Passwort) VALUES ('" + username + "' , '" + Passwort + "');");
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
			return false;
		}
		return true;
	}
	

	public boolean anmelden(String username, String Passwort) {
		String sql = "SELECT count(*) FROM Account WHERE Name = '"+ username +"';";
		String sql2 = "SELECT count(*) FROM Account WHERE Passwort = '"+ Passwort +"';";

		try {
			Class.forName(driver);
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			Statement stmt2 = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			ResultSet rs2 = stmt2.executeQuery(sql2);
			while (rs.next()&& rs2.next()) {
				if (rs.getInt(1) == 0 && rs2.getInt(1) == 0) 
					return false;
				else 	
					return true;
				
					
		
			}
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return false;
	}
	public void KisteKaufen(String Kiste_ID, String ACC_ID, String wert) {
		String sql = "INSERT INTO Inventar (F_Kisten_ID, F_Acc_Id) VALUES('"+ Kiste_ID + "','" + ACC_ID + "');";
		String sql2 = "Update Account Set Geld = Geld - '" + wert +"' WHERE Acc_ID = '" + ACC_ID + "'";
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement ps = con.prepareStatement(sql);
			PreparedStatement ps2 = con.prepareStatement(sql2);
			ps.executeUpdate();
			ps2.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
			System.out.println("Fehler mit SQL-Befehl: " + sql2);
		}
	}

	public void ItemKaufen(String Item_ID, String ACC_ID,String wert) {
		String sql = "INSERT INTO Inventar (F_Item_ID, F_Acc_Id) VALUES('"+ Item_ID + "', '" + ACC_ID + "');";
		String sql2 = "Update Account Set Wert = Wert - '" + wert +"' WHERE Acc_ID = '" + ACC_ID + "'";
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();
			PreparedStatement ps2 = con.prepareStatement(sql2);
			ps2.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
			System.out.println("Fehler mit SQL-Befehl: " + sql2);
		}
	}

	public void ItemVerkaufen(String Item_ID, String ACC_ID, String wert) {
		String sql = "Delete from Inventar Where F_Acc_Id = '" + ACC_ID + "' AND F_Item_ID = '" + Item_ID + "');";
		String sql2 = "Update Account Set Wert = Wert + '" + wert +"' WHERE Acc_ID = '" + ACC_ID + "'";
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();
			PreparedStatement ps2 = con.prepareStatement(sql2);
			ps2.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
			System.out.println("Fehler mit SQL-Befehl: " + sql2);
		}
	}
	public void KisteVerkaufen(String Kiste_ID, String ACC_ID, String wert) {
		String sql = "Delete from Inventar Where F_Acc_Id = '" + ACC_ID + "' AND F_Kisten_ID = '" + Kiste_ID + "');" ;
		String sql2 = "Update Account Set Wert = Wert + '" + wert +"' WHERE Acc_ID = '" + ACC_ID + "'";
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();
			PreparedStatement ps2 = con.prepareStatement(sql2);
			ps2.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
			System.out.println("Fehler mit SQL-Befehl: " + sql2);
		}
	}

	public String[][] getInventar(String Acc_ID) {
		String sql = "SELECT I_Name, Seltenheitgrad, Wert FROM Items INNER JOIN Inventar ON ID = F_Item_ID INNER JOIN Account ON Acc_ID = F_Acc_ID WHERE F_Acc_ID = " + Acc_ID + ";  " ;
		String sql2 ="SELECT K_Name, Seltenheitgrad, Wert FROM kisten INNER JOIN inventar ON Kisten_ID = F_Kisten_ID INNER JOIN Account ON Acc_ID = F_Acc_ID WHERE F_Acc_ID =" + Acc_ID + ";";
		List<String[]> ergebnisliste = new Vector<String[]>();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next()) {
				String[] datensatz = new String[50];
				for(int i = 0; i<50; i++)
				datensatz[i] = rs.getString(3);
				ergebnisliste.add(datensatz);
			}
			ResultSet rs2 = s.executeQuery(sql2);
			while (rs2.next()) {
				String[] datensatz = new String[50];
				for(int i = 0; i<50; i++)
				datensatz[i] = rs2.getString(3);
				ergebnisliste.add(datensatz);
			}
	
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
		String[][] playlist = new String[ergebnisliste.size()][50];
		int i = 0;
		for (String[] s : ergebnisliste)
			playlist[i++] = s;
		return playlist;
	}

	public String[][] zeigeItems() {
		String sql = "SELECT * FROM Items ORDER BY Wert;";
		List<String[]> Itemliste = new Vector<String[]>();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next()) {
				String[] datensatz = new String[50];
				for(int i = 0; i<50; i++)
				datensatz[i] = rs.getString(3);
				Itemliste.add(datensatz);
			}		
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
		String[][] playlist = new String[Itemliste.size()][50];
		int i = 0;
		for (String[] s : Itemliste)
			playlist[i++] = s;
		return playlist;
	}
	
	public String[][] zeigekisten() {
		String sql = "SELECT * FROM Kisten ORDER BY Wert;";
		List<String[]> Itemliste = new Vector<String[]>();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next()) {
				String[] datensatz = new String[50];
				for(int i = 0; i<50; i++)
				datensatz[i] = rs.getString(3);
				Itemliste.add(datensatz);
			}		
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
		String[][] playlist = new String[Itemliste.size()][50];
		int i = 0;
		for (String[] s : Itemliste)
			playlist[i++] = s;
		return playlist;
	}
	
	public String zeigeGold(String ACC_ID){
		String sql = "SELECT Geld FROM account WHERE Acc_ID = '" + ACC_ID + "';";
		String Datensatz = null;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
			 Datensatz = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
		return Datensatz;
	}
	
	public String ItemGold(String Item_ID){
		String sql = "SELECT Wert FROM items WHERE ID = '" + Item_ID + "';";
		String Datensatz = null;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
			 Datensatz = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
		return Datensatz;
	}
	
	public String KistenGold(String Kisten_ID){
		String sql = "SELECT Wert FROM items WHERE Kisten_ID = '" + Kisten_ID + "';";
		String Datensatz = null;
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
			 Datensatz = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler mit SQL-Befehl: " + sql);
		}
		return Datensatz;
	}
	
	
	 
	 public String[][] getKiste(String Kisten_ID) {
			String sql = "SELECT I_Name, Seltenheitgrad, Wert FROM Items INNER JOIN Kisten_items ON ID = Item_ID WHERE F_Kisten_ID = " + Kisten_ID + ";  " ;
			List<String[]> ergebnisliste = new Vector<String[]>();
			try {
				Class.forName(driver);
				Connection con = DriverManager.getConnection(url, user, password);
				Statement s = con.createStatement();
				ResultSet rs = s.executeQuery(sql);
				while (rs.next()) {
					String[] datensatz = new String[50];
					for(int i = 0; i<15; i++)
					datensatz[i] = rs.getString(3);
					ergebnisliste.add(datensatz);
				}		
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Fehler mit SQL-Befehl: " + sql);
			}
			String[][] playlist = new String[ergebnisliste.size()][15];
			int i = 0;
			for (String[] s : ergebnisliste)
				playlist[i++] = s;
			return playlist;
		}
		public String zeigeItemBild(String item_ID, int width, int height, JLabel Label2){
			String sql = "SELECT I_Name, Seltenheitgrad, Wert, Bild FROM items WHERE ID = '" + item_ID + "';";
			String Datensatz = null;
			try {
				Class.forName(driver);
				Connection con = DriverManager.getConnection(url, user, password);
				Statement s = con.createStatement();
				ResultSet rs = s.executeQuery(sql);
				if(rs.next()) {
					byte[] img = rs.getBytes("Bild");
					ImageIcon image = new ImageIcon(img);
					Image im = image.getImage();
					Image myImg = im.getScaledInstance(width, height, Image.SCALE_SMOOTH);
					ImageIcon newImage = new ImageIcon(myImg);
					Label2.setIcon(newImage);
				}
				while(rs.next()) {
				 Datensatz = rs.getString(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Fehler mit SQL-Befehl: " + sql);
			}
			return Datensatz;
	    }
		
		public String zeigeItem(String item_ID){
			String sql = "SELECT I_Name, Seltenheitgrad, Wert FROM items WHERE ID = '" + item_ID + "';";
			String Datensatz = null;
			try {
				Class.forName(driver);
				Connection con = DriverManager.getConnection(url, user, password);
				Statement s = con.createStatement();
				ResultSet rs = s.executeQuery(sql);
				while(rs.next()) {
				 Datensatz = rs.getString(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Fehler mit SQL-Befehl: " + sql);
			}
			return Datensatz;
	    }
		
		public String zeigeKiste(String Kiste_ID){
			String sql = "SELECT I_Name, Wert FROM items WHERE Kisten_ID = '" + Kiste_ID + "';";
			String Datensatz = null;
			try {
				Class.forName(driver);
				Connection con = DriverManager.getConnection(url, user, password);
				Statement s = con.createStatement();
				ResultSet rs = s.executeQuery(sql);
				while(rs.next()) {
				 Datensatz = rs.getString(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Fehler mit SQL-Befehl: " + sql);
			}
			return Datensatz;
	    }
}

